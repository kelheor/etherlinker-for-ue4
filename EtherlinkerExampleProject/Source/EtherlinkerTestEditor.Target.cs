using UnrealBuildTool;
using System.Collections.Generic;

public class EtherlinkerTestEditorTarget : TargetRules
{
	public EtherlinkerTestEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
        DefaultBuildSettings = BuildSettingsVersion.Latest;
        IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_4;
        ExtraModuleNames.Add("EtherlinkerTest");
	}
}
