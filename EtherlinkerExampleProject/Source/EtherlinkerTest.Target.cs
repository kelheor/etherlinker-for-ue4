using UnrealBuildTool;
using System.Collections.Generic;

public class EtherlinkerTestTarget : TargetRules
{
	public EtherlinkerTestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.Latest;
        IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_4;
        ExtraModuleNames.Add("EtherlinkerTest");
	}
}
